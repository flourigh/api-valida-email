<?php
namespace App\Http\Controllers;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ServiceValidatorController extends BaseController
{

    public function emailValidator(Request $request)
    {

        $data = $request->toArray();

        $validator = new EmailValidator();
        $multipleValidations = new MultipleValidationWithAnd([
            new RFCValidation(),
            new DNSCheckValidation(),
        ]);
        $response = $validator->isValid($data["email"], $multipleValidations); //true

        return response()->json([
            "email" => $data["email"],
            "status" => $response,
        ], 200);

    }
}
